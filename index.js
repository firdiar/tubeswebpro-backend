// Initialize Firebase
const admin = require('firebase-admin');

const serviceAccount = require("path/to/serviceAccountKey.json");

let config = require('config/config-firebase.json');
config.credential = admin.credential.cert(serviceAccount);

admin.initializeApp(config);
const database = admin.database();
const refCount = database.ref("users/userCount");

// Initialize Express
const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const port = process.env.PORT || 3000;

let responBack = null;

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json())
app.use(express.static('public'));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

const generateRandomToken = function(length) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

const editUserCount = function(changes) {
  refCount.once('value').then(function(snapshot) {
    refCount.set(snapshot.val() + changes);
  });
}



//API function
app.get('/' , (req , res) => {
	res.send('Welcome To Frima');
})

//Api Register
app.post('/api/register' , (req , res) => {
			let ref = database.ref("users/"+req.body.username);
			responBack = { 	respon : "",
											isSuccess : false};
			ref.once('value').then(function(snapshot) {
					if(snapshot.val() != null){
							responBack.respon = "Username Already Exist";
					}else {
              responBack.isSuccess = true;

              let userBaseData = require('base-outlines/user-data.json');
              userBaseData.password = req.body.password;

              userBaseData.public = req.body.public;

              ref.set(userBaseData);
              editUserCount(1);
              responBack.respon = "User created successfully";

          }
					res.send(responBack);
          responBack = null;
			});
})


//Api Login
app.post('/api/login' , (req , res) => {
			let ref = database.ref("users/"+req.body.username);
			responBack = { 	token : "",
                      respon : "",
											isSuccess : false};
			ref.once('value').then(function(snapshot) {
					if(snapshot.val() != null && snapshot.val().password == req.body.password){
							responBack.token = generateRandomToken(20);
              responBack.respon = "Valid username and password";
							responBack.isSuccess = true;
							ref.child('token').set(responBack.token);
					}else if(snapshot.val() == null){
              responBack.respon = "User not found";
          }else if(snapshot.val().password != req.body.password){
              responBack.respon = "Wrong password";
          }
					res.send(responBack);
          responBack = null;
			});
})


//Api Get Public User Data
app.get('/api/users/:username' , (req , res) => {
			let ref = database.ref("users/"+req.params.username+"/public");
			ref.once('value').then(function(snapshot) {
          responBack = snapshot.val();
					res.send(responBack);
          responBack = null;
			});

})


// Down Here is just prototype
app.get('/api/home/:userId' , (req , res) => {

	res.send([{yourlast:req.params.userId}]);
})

app.get('/api/data/:userId' , (req , res) => {
	let ref = database.ref("users/"+req.params.userId);
	var respon = { 	id : req.params.userId,
									username : "",
									password : ""};
	ref.once('value').then(function(snapshot) {
		  respon.username = (snapshot.val() && snapshot.val().username) || null;
			respon.password = (snapshot.val() && snapshot.val().password) || null;
		  res.send(respon);
	})
})



app.get('/api/delete' , (req , res) => {

	database.ref("users/").remove();
	res.send('delete success');
})


app.listen(port, ()=> console.log('listening on 3000'));
