# Frima Api Backend
["https://tubeswebpro-backend.herokuapp.com"](https://tubeswebpro-backend.herokuapp.com)


## What is an API
API is the acronym for Application Programming Interface, which is a software intermediary that allows two applications to talk to each other. Each time you use an app like Facebook, send an instant message, or check the weather on your phone, you’re using an API.

## Summary
Frima is task from our lecture to make or build our own website, frima using
API system like other API, but because is for a web i just make post and get

## Usage
Api is easy to use, you can search on the google "<your framework> get http request" or something like that
for example "ajax get http request" >> ["How Get Http Request From Ajax"](https://www.w3schools.com/jquery/jquery_ajax_get_post.asp)

## Testing
There are many way to test api, the simplest way is just browse the link, lets try this
```
https://tubeswebpro-backend.herokuapp.com/api/home/inidiganti-aja
```
but if you just browse, you cant test other function like POST

so if you want to try all of this API functionality, use postman apps, you can download it below
["Download Postman"](https://www.getpostman.com/downloads/)

## Ready API
Ready HTTP request for this Api was listed below

### Register [POST]
Create new user for login website

Request need
```
URL : https://tubeswebpro-backend.herokuapp.com/api/register
```
```
Body: {
    "username": "your username in app",
    "password": "your password in app",
    "public" : {"desc":"kosong","email":"kosong@gmail.com","gender":"Laki-Laki","nama":"kosong"}
}
```
Response result
```
{
    "respon": <error message> (string),
    "isSuccess": <status success register> (boolean)
}
```


### LOGIN [POST]
Validate user for log in the website and get Token code access

Request need
```
URL : https://tubeswebpro-backend.herokuapp.com/api/login
```
```
Body: {
    "username": "your username in frime",
    "password": "your password in frime"
}
```
Response result
```
{
    "token": <random token> (string),
    "respon": <error message> (string),
    "isSuccess": <status success login> (boolean)
}
```

### Getting User Public Data [GET]
Get specific user public data from the database

Request need
```
URL : https://tubeswebpro-backend.herokuapp.com/api/users/:username
```
Response result
```
{
    "name": <name of user> (string),
     ...  <other data , not set up yet>
}
```

## Test API
Just test HTTP request for this Api was listed below

### Test001 [GET]
Request need
```
URL : https://tubeswebpro-backend.herokuapp.com/api/home/<feel free to change>
```
Response result
```
[{
    yourlast: <that you fill in <feel free to change>>
}]
```
